FROM nginx:1.18-alpine

RUN apk update
RUN apk add nginx-mod-http-headers-more

# Required on Child Docker
#COPY build /usr/share/nginx/html

COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]